# Compiling the front-end of a Server add-on

Atlassian's P2 plugin system offers you incredible flexibility to build just about anything you would want to for a Server product.
This power is available to you... assuming you know how some of the foundations underpinning P2 work.

By following this tutorial, you'll learn how to add a compilation step for your front-end code when building a Server add-on. This will unblock your ability to use more modern front-end technologies to build your add-on's UI.

## How to get the most out of this repository

The codebase aims to demonstrate *how* you can compile your front-end code, *not the technologies to do so*. The front-end technology choices made here can be substituted for your own -- e.g., [TypeScript][105] instead of ES2015, [Grunt.js][106] instead of Gulp.js, [NPM][107] instead of [Yarn][108], and so on.

The commit history in this repository has been carefully crafted and laden with commentary. *Take a moment to read the Git history!*
Each individual commit is designed to demonstrate a step in the journey from a reasonably complex yet static front-end,
through to having a build-time compilation, to also compiling and making your new code available in-browser whenever the
source changes during product runtime.

## The journey, in three parts

Ideally, we want our devloop to be as short as possible. Modern front-end technologies like ES2015, React, JSX or postcss
are huge time-savers for building sophisticated UI... but they often need to be compiled
in to something the browser understands.

The challenge we're focussing on in this tutorial is: "How can I build my add-on using a modern front-end technology"?

To restate this challenge as some goals:

* We want our front-end to be compiled at the plugin's build-time.
* Our compiled front-end code should be served at product runtime.
* In our devloop, we should be able to update our source, then reload the browser to see that change immediately.

We'll achieve this in three discrete sections.

* [Part One: compiling the code][301]
* [Part two: serving compiled code at runtime][302]
* [Part three: save-and-reload of compiled code at runtime][303]

[101]: https://github.com/lukehoban/es6features
[102]: https://en.wikipedia.org/wiki/ECMAScript#5th_Edition
[103]: https://babeljs.io/
[104]: http://gulpjs.com/
[105]: https://www.typescriptlang.org/
[106]: https://gruntjs.com/
[107]: https://docs.npmjs.com/getting-started/what-is-npm
[108]: https://yarnpkg.com/en/docs/getting-started
[301]: 03-compiling-ui-code.md
[302]: 04-serve-compiled-ui-code.md
[303]: 05-save-reload-compiled-ui-code.md