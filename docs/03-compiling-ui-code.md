# Compiling the front-end of a Server add-on
## Part One: Setting up a compilation step for your front-end code

In this section, we'll explore how to add compilation steps for an Atlassian P2 plugin's front-end code.

By the end of the tutorial, running `atlas-mvn package` on your plugin's source will result in the
front-end assets being run through a build step for you.

### Preface: Decide what you want to compile (and why)

Before adding any compilation steps to your codebase, consider why you want them there at all.

* Are you in need of future spec-compliant language features (e.g., ECMAScript's `async` and `await` keywords)?
* Are you attempting to use a framework or library that requires you to compile your source (e.g., using JSX with
  React.js, or TypeScript with Angular)?
* Are you trying to refactor and improve your codebase for clarity (e.g., to remove boilerplate or terse code patterns)?
* Are you attempting to improve maintainability of your code? (e.g., adding LESS / SASS to make use of variables or
  calculations for values)?

Depending on when you start your project, the browsers of the day may already support all of the things you were
hoping to achieve by compiling your code.
If that's the case, why would you want to add the complexity of yet another toolchain and build step to your project?

Consider why and whether a compilation step is necessary for your project's front-end code.
More than anything else, knowing *why* you're doing this will save you the most time in the long run.

### The beginning: adding your build tool to your project

There are a plethora of front-end build tools today.
Most of these tools are built upon [Node.js][200] as their runtime.
Assuming this holds true for the tools you want to use, the first step is to add
Node.js as a dependency of your project.

#### Add a dependency on Node.js

To signify your project requires Node.js, add [a `package.json` file][201] to the root of your project.
It's a good idea to fill in [the `"engines"` section][202] of your `package.json` with the version of Node.js you
intend to use.
By setting the version explicitly, any other developer (or [continuous integration tool][101]
like [Bitbucket Pipelines][102]) will use the same version as you, thus avoiding any potential
incompatibility problems in the future.

In this tutorial, I chose Node.js version 6.9.0, because it is a long-term support (LTS) version.

```json
{
  // ...
  "engines": {
    "node": "6.9.0"
  }
}
```

#### Add a dependency on a Node.js package manager

Next up, you'll want to use a package manager in order to automate the downloading and installation of
your front-end build tool.

At the time of writing, the two options were [NPM][203] or [Yarn][204]. I chose Yarn because of its fast download
times and reliable support for downloading the same version of dependencies across machines.

Add the latest version of your package manager to the `"engines"` section of your `package.json` file.

```json
{
  // ...
  "engines": {
    "node": "6.9.0",
    "yarn": "0.21.3"
  }
}
```

#### Add a front-end build manager as a project dependency

Sometimes, your build tools will be simple enough that you could add them as a single-line script. Over time, however,
as your project evolves, it's less likely that this will continue to be the case. To manage the steps necessary to
turn your front-end source in to production code, it's a good idea to add a front-end build manager to your project.

In this tutorial I chose to use [Gulp 4][205]. Gulp allows you to compose simple functions in to powerful
chains of steps to manage the entire source-to-production build process.

From a terminal window, run either `yarn add --dev gulp@4` or `npm install --save-dev gulp@4` to add it to your project.

Though you are free to choose your own build manager (or use none at all), the remainder of this tutorial
assumes you're using Gulp 4.


### The actual building bit: Defining the compilation step for your code

Because we're building an Atlassian P2 plugin, we've got Maven as our primary build tool.
So that the development process for our plugin is no different to a regular one, we will want to ensure that any
time our P2 plugin gets built (i.e., when either the `atlas-mvn install` or `atlas-mvn package` commands are run),
the front-end is also built. To do that, we need to "glue" our front-end build pipeline together with Maven.

#### Add the `frontend-maven-plugin` to the project

[The `frontend-maven-plugin`][206] is a way of invoking Node.js-based tools via Maven.
It will be instrumental in compiling your front-end.
To use it, you need to add it as a "build" dependency in your project's `pom.xml` file.

```xml
<project>
  <!-- ... -->
  <build>
    <plugins>
      <plugin>
        <groupId>com.github.eirslett</groupId>
        <artifactId>frontend-maven-plugin</artifactId>
        <version>1.4</version>
      </plugin>
      <!-- other build plugins, etc. -->
    </plugins>
  </build>
  <!-- and so on... -->
</project>
```

Once added to the `<build>` section, you'll need add configure the plugin
to download and install the Node-based dependencies. To the `<plugin>` block, you'll add a `<configuration>`
section that lists the same versions of Node.js and your package manager that you listed in the
`"engines"` section of your `project.json` file.

```xml
  <plugin>
    <groupId>com.github.eirslett</groupId>
    <artifactId>frontend-maven-plugin</artifactId>
    <version>1.4</version>
    <configuration>
      <installDirectory>${project.build.directory}</installDirectory>
      <nodeVersion>v6.9.0</nodeVersion>
      <yarnVersion>v0.21.3</yarnVersion>
    </configuration>
  </plugin>
```

If your plugin contains more than one Maven project, you can keep your code [DRY][103] by moving
this configuration section out in to [the `<dependencyManagement>` section][207] of your project's root `pom.xml` file.

At the time of writing, the duplication of the version info for Node is an unfortunate necessity.
The [DRY][103]-est developers can choose to omit the `"engines"` section from the `package.json` file;
ultimately the front-end build process will be driven by Maven, so it makes sense for Maven to be the
canonical source of truth in scenarios where there would be a double-up of information.

#### Hook the front-end build manager in to Maven

Now that the `frontend-maven-plugin` is in place, you can use it to invoke your front-end build pipeline.

First, add some "executions" to the `frontend-maven-plugin` in order to download and install a project-local version of
Node and your package manager.

```xml
<!-- pom.xml -->
  <plugin>
    <groupId>com.github.eirslett</groupId>
    <artifactId>frontend-maven-plugin</artifactId>
    <!-- any configuration you may need -->
    <executions>
        <execution>
            <id>step-1-set-up-environment</id>
            <goals>
                <goal>install-node-and-yarn</goal>
            </goals>
            <phase>initialize</phase>
            <inherited>false</inherited>
        </execution>
        <execution>
            <id>step-2-install-frontend-dependencies</id>
            <goals>
                <goal>yarn</goal>
            </goals>
            <configuration>
                <arguments>install --verbose</arguments>
            </configuration>
            <phase>initialize</phase>
            <inherited>false</inherited>
        </execution>
    </executions>
  </plugin>
```

Whether you're using Gulp or just plain scripts for your front-end build management, you should hook them up in the
same way: by executing a script in the `"scripts"` section of your `package.json` file.

In this codebase, I've chosen to write a function in my Gulpfile named "processResources",
which will manage all the main front-end code processing.
This function is then referenced in a gulp task named "build".
The gulp task is invoked via a `package.json` script called "build".
Finally, I have "glued" this script in to Maven's build lifecycle via an
execution for the `frontend-maven-plugin`.

```js
// gulpfile.js

function processOtherStuff() {
    // this is implemented in the next section
}

// this is implemented in the next section
var processOurJs = gulp.series();

var processResources = gulp.parallel(processOurJs, processOtherStuff);

gulp.task("build", processResources);
```

```json
// package.json
{
  // ...
  "scripts": {
    "build": "gulp build" // references the gulp task named "build"
  }
  // ...
}
```

```xml
<!-- pom.xml -->
  <plugin>
    <groupId>com.github.eirslett</groupId>
    <artifactId>frontend-maven-plugin</artifactId>
    <!-- any configuration you may need -->
    <executions>
      <!-- the first two executions are still here -->
        <execution>
            <id>step-3-compile-the-ui</id>
            <goals>
                <goal>yarn</goal>
            </goals>
            <configuration>
                <arguments>build</arguments><!-- references the package.json script called "build" -->
            </configuration>
            <phase>process-resources</phase>
        </execution>
    </executions>
  </plugin>
```

There are a few good reasons you should invoke your scripts via the `package.json` file:

1. It guarantees that when the `frontend-maven-plugin` runs, it will use the locally-installed
   Node and package manager versions instead of any globally-installed version.

2. It also guarantees that the locally-installed development dependencies will be used instead of global ones.

3. Finally, it means you can test or invoke your front-end build pipeline independently of running Maven. This
   decoupling will make extending and debugging your front-end compilation much easier ;)

If your CI environment or a fellow developer seems to be having compilation issues,
check whether the build was using a global or local version of the build tool!

## Next steps

At this point, we're able to compile our plugin's front-end code by running `atlas-mvn package`.
Of course, at this stage, we haven't actually compiled any front-end code -- we've just set up the
boilerplate to allow it to happen.

In [the next section][901], we'll run through a few examples for using some common
front-end tools and technologies, along with some tips and gotchas for each.

[101]: https://www.thoughtworks.com/continuous-integration
[102]: https://bitbucket.org/product/features/pipelines
[103]: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
[200]: https://nodejs.org
[201]: https://docs.npmjs.com/files/package.json
[202]: https://docs.npmjs.com/files/package.json#engines
[203]: https://docs.npmjs.com/getting-started/what-is-npm
[204]: https://yarnpkg.com/en/docs/getting-started
[205]: https://github.com/gulpjs/gulp/tree/4.0
[206]: https://github.com/eirslett/frontend-maven-plugin
[207]: https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html#Dependency_Management
[901]: 04-serve-compiled-ui-code.md
