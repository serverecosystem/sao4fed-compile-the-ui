# Why compile the front-end?

As productive developers, we're constantly looking for ways we can reduce the amount of time it takes to build valuable things for our customers. One such way is to use modern front-end technologies that facilitate building rich, performant front-end applications.

Historically, front-end development was limited by the technologies browsers understood. It was assumed that your source code had to be the same code that the browser would download, parse, and execute.

Today, the browsers can be treated more like a compilation target. There are a wide variety of technologies, tools, and techniques to compile and transpile your code in to what the browsers understand. The restrictions on how developers can write their code has been somewhat alleviated.

When it comes to the Atlassian Ecosystem, it has not been readily apparent how those technologies apply to building add-ons for Atlassian products.

This tutorial aims to shed some light on building an add-on with more modern front-end technologies.
