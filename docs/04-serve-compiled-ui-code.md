# Compiling the front-end of a Server add-on
## Part Two: serving compiled front-end code at runtime

Whether we're building a traditional website or a standalone web application, if we
build for the web, our goal is always the same: have a set of HTML, CSS, JavaScript, and
other assets we can serve to a web browser and have the browser understand.

The front-end applications of today are beginning to treat these technologies as basic building blocks,
and often use technologies and tooling that compile the source application code
in to that basic HTML, CSS and JS.

When building Atlassian P2 plugins, we're also able to use the same front-end technologies and tools.
However, because application ships within an Atlassian Server product, we can't use them without first
configuring them. We need to do some groundwork to make sure the tooling we pick will compile our
ultimate HTML, CSS and JS in to the right place, such that they end up in the running product.

### Preface: Installing your build tools as project dependencies

In this section of the tutorial, it is assumed you have followed [the previous section on compiling
UI code][900], which means your project will be configured to build code using [Gulp 4][202]
via [Yarn][201] and [Node.js][200].

For each piece of code you intend to compile, you'll need to install a build tool to help you do it.
You'll install each tool using either `yarn add --dev X` or `npm install --save-dev X` to ensure they're
available as dependencies of your project.

### Where does front-end code need to be compiled to?

An Atlassian P2 plugin follows the directory structure of a standard [Maven][103] project. What this means for you is
that, roughly speaking:

* All the front-end assets for your plugin are expected to be in the `src/main/resources` folder, and
    * Those assets are expected to be static!
* When the plugin is built, the front-end assets should end up in the `target/classes` directory, alongside the
  other compiled code and metadata that constitutes your plugin.
* At product runtime, these files will be retrieved

To compile your front-end assets, you'll need to do two things.

1. Tell Gulp where the source and compiled code should live by adding some constants to your gulpfile:

```js
// Some constants for defining where the source and compiled code
// of an Atlassian P2 plugin typically live.
var MVN_OUTPUT_DIR = path.join('target', 'classes');
var FRONTEND_SRC_DIR = path.join('src', 'main', 'resources');
```

2. Tell Maven that it should compile the code in your front-end source directory (which, if you
   followed [the previous section of this tutorial][900], you've already done!)

If you're interested in a more in-depth look at the way an Atlassian P2 plugin is structured, you can read
the [front-end pieces of an add-on][902] documentation, or [watch the how a Server add-on works section][903] of
the "Server Add-Ons for Front-End Developers" talk from AtlasCamp 2017 on Youtube.

### How to compile various things

#### Minifying JavaScript

When you build a typical Atlassian P2 plugin, by default, [AMPS][301] will assume you want all of your
JavaScript code to be run through a minifier, so that there's less code to download on the client-side.

The default tooling provided by AMPS to minify code is adequate for projects using [ECMAScript 5 (aka ES5)][100],
but it has some problems:

* It does not understand any newer flavours of JavaScript, such as [ECMAScript 2015 (aka ES6)][101],
  [TypeScript][102], etc.
* It doesn't include more modern code optimisation features, such as mangling or scope hoisting.

Because of this, it's a good idea to start your compilation journey by
adding a minification step to your own build pipeline.

Assuming you want to minify vanilla JavaScript code, [UglifyJS][206] is a good tool to start with.

First, run either `yarn add --dev uglify` or `npm install --save-dev uglify` to add UglifyJS to
your project's dependencies.

Next, add a Gulp task to run minification over your JavaScript source code.

```js
var jsFiles = path.join(FRONTEND_SRC_DIR, '**', '*.js');

function minifyJs() {
    return gulp.src(jsFiles)
        .pipe(uglify())
        .pipe(rename({ suffix: '-min' }))
        .pipe(gulp.dest(MVN_OUTPUT_DIR));
}

var processResources = gulp.parallel(minifyJs);

```

Altogether, your gulpfile might look something like this:

```js
var gulp = require('gulp');
var path = require('path');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

// Some constants for defining where the source and compiled code
// of an Atlassian P2 plugin typically live.
var MVN_OUTPUT_DIR = path.join('target', 'classes');
var FRONTEND_SRC_DIR = path.join('src', 'main', 'resources');

var jsFiles = path.join(FRONTEND_SRC_DIR, '**', '*.js');

function minifyJs() {
    return gulp.src(jsFiles)
        .pipe(uglify())
        .pipe(rename({ suffix: '-min' }))
        .pipe(gulp.dest(MVN_OUTPUT_DIR));
}

var processResources = gulp.parallel(minifyJs);

gulp.task('build', processResources);
```

Give it a test by running `atlas-mvn clean && gulp build`. If everything's working well, you should see some
JavaScript files in your `target/classes` directory, along with a `-min.js` variant of each one.

Finally, you need to disable AMPS' own minification.

Add the following configuration to the `maven-amps-plugin` your `pom.xml` file:

```xml
    <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>maven-confluence-plugin</artifactId>
        <version>6.2.11</version>
        <extensions>true</extensions>
        <configuration>
            <!-- this will turn off AMPS' compression of JavaScript resources. -->
            <compressResources>false</compressResources>
        </configuration>
    </plugin>
```

Now, if you run `atlas-mvn clean package`, your minification step should run, and you should see the
same files output as you did when you ran `gulp build` manually.

##### How Atlassian serves minified code at runtime

When an Atlassian product sees JavaScript files with a `-min` suffix in a plugin file, it will use those files instead
of the standard files in production. In development mode, by default, the unminified files will be served. If, however,
you are using [the Quickreload plugin][302], you can modify this behaviour and toggle minification on/off. Read the
QuickReload plugin's documentation for more on how to do this.

We'll be talking a little bit more about Quickreload in the next section of this tutorial.

#### Transpiling JavaScript using Babel

Let's say that, like this tutorial plugin, you want to use the latest ECMAScript features in your codebase
and then convert in to valid [ES5][100] (which at the time of writing is the latest version of JS
that all browsers understand).

To do that, you'll need to add [Babel][205] and a few of its plugins to your project.

##### Adding Babel to the project

Run either `yarn add --dev babel babel-preset-es2015 babel-register` or
`npm install --save-dev babel babel-preset-es2015 babel-register`; this will add the requisite pieces of Babel to
transpile your code in to valid ES5.

Create a `.babelrc` file in the root of your project. For the moment, add the following to the file:

```json
{
  "presets": ["es2015"]
}
```

Next, we need to add some configuration for Babel to make it work properly with an Atlassian P2 plugin.

##### Compatibility and gotchas with Atlassian products

The presets for Babel will transpile every ES2015 feature in to valid ES5 syntax. It will
also make a few "safe" optimisations on your behalf. Unfortunately, as these presets can change over time, and
because some transforms are incompatible with the way Atlassian products work, for now, it is safer to
manually configure the transforms you need.

* You need to ensure that the `transform-strict-mode` plugin is not used, as if any `"use strict"` statements were
  hoisted to the top of a file, they would apply to an entire batch file in an Atlassian product's production mode,
  which can cause unexpected problems with non-strict code.

* Atlassian products may not ship with the compatibility layer necessary to support ES6 features like the
new prototype methods on Array. Shipping the compatibility layer for these features is beyond the scope of
this tutorial, so it's recommended you avoid use of the `babel-polyfill` module
or the `babel-plugin-transform-runtime` plugin.

The [`.babelrc` file in this tutorial code][400] is a good starting point for enabling the safe-to-use ES2015 features.

##### Adding Babel to the front-end build pipeline

You can make an addition to your gulpfile's `processOurJs` function to ensure that code is transpiled and minified:

```js
var jsFiles = path.join(FRONTEND_SRC_DIR, '**', '*.js');

function transpileAndMinifyJs() {
    return gulp.src(jsFiles)
        .pipe(babel())
        .pipe(gulp.dest(MVN_OUTPUT_DIR))
        .pipe(uglify())
        .pipe(rename({ suffix: '-min' }))
        .pipe(gulp.dest(MVN_OUTPUT_DIR));
}

var processOurJs = gulp.series(transpileAndMinifyJs);
```

Now, your full gulpfile may look something like this:

```js
var babel = require('gulp-babel');
var gulp = require('gulp');
var path = require('path');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

// Some constants for defining where the source and compiled code
// of an Atlassian P2 plugin typically live.
var MVN_OUTPUT_DIR = path.join('target', 'classes');
var FRONTEND_SRC_DIR = path.join('src', 'main', 'resources');

var jsFiles = path.join(FRONTEND_SRC_DIR, '**', '*.js');

function transpileAndMinifyJs() {
    return gulp.src(jsFiles)
        .pipe(babel())
        .pipe(gulp.dest(MVN_OUTPUT_DIR))
        .pipe(uglify())
        .pipe(rename({ suffix: '-min' }))
        .pipe(gulp.dest(MVN_OUTPUT_DIR));
}

var processResources = gulp.parallel(transpileAndMinifyJs);

gulp.task('build', processResources);
```

Give it a test by running `atlas-mvn clean && gulp build`. If everything's working well, you should see some
compiled JavaScript files in your `target/classes` directory, where any ES6 syntax from your source has been
converted in to valid ES5!

Now that you know your gulp task is working correctly, check that the whole build works properly by running
`atlas-mvn clean package`. The gulp build will be invoked during the Maven build lifecycle, and thus your
JavaScript will now be transpiled every time your plugin is built!

## Next steps

So now you can use a modern JavaScript language at build-time, and you're happy that you can use modern front-end
technologies when building your plugin.

What about compiling your styles and templates?
Happily, the knowledge you've gained about how to compile your JavaScript in to your P2 plugin can all be applied
to compiling your styles and templates at build-time, too!

But, what about being able to increase your development loop speed? If all you do is compile your code at build-time,
when you're developing your plugin, you would need to be building your plugin after every change just to see the effect
in a running Atlassian product. To improve upon that, head to [the next section of this tutorial][904], which
runs through [how to enable save and reload for your compiled front-end code][904].

What about modern front-end code management? And what about building a more optimal and performant front-end?
You can learn more about this in the next tutorial, [Bundling server add-on UIs with Webpack][905].


[100]: http://kangax.github.io/compat-table/es5/
[101]: http://kangax.github.io/compat-table/es6/
[102]: https://www.typescriptlang.org/
[103]: https://maven.apache.org/
[200]: https://nodejs.org
[201]: https://yarnpkg.com/en/docs/getting-started
[202]: https://github.com/gulpjs/gulp/tree/4.0
[205]: https://babeljs.io/
[206]: http://lisperator.net/uglifyjs/
[301]: https://bitbucket.org/atlassian/amps
[302]: https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload
[400]: ../.babelrc
[900]: 03-compiling-ui-code.md
[901]: 05-save-reload-compiled-ui-code.md
[902]: https://bitbucket.org/serverecosystem/sao4fed-the-frontend-pieces-of-an-addon
[903]: https://youtu.be/2yf-TzKerVQ?t=17m35s
[904]: 05-save-reload-compiled-ui-code.md
[905]: https://bitbucket.org/serverecosystem/sao4fed-bundle-the-ui
