# Compiling the front-end of a Server add-on
## Part Three: save-and-reload of compiled code at runtime

In [the previous section][900], you learned how to add compilation steps
for a few kinds of resources, and how to test that the compilation works.
Now, there's one more important piece of the puzzle to complete: loading
those compiled resources quickly when developing your plugin.

### Changing how the plugin resources will be discovered

Just about you need to change will happen in the `pom.xml` file of your project.
Specifically, within the `<configuration>` of the `maven-amps-plugin`:

```xml
    <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>maven-confluence-plugin</artifactId>
        <version>6.2.11</version>
        <extensions>true</extensions>
        <configuration>
            <!-- all of our development-time and production-time alterations
                 will be somewhere in here -->
        </configuration>
    </plugin>
```

### Loading compiled front-end code at development time

There's an important step to take in order to convince an Atlassian product to load your compiled code. It
has to do with how AMPS uses and extends the Maven project structure conventions.

While Maven itself doesn't make any assumptions about the code you put in the resources directory, AMPS does:
AMPS assumes that code in the resources directory is production-ready!

The consequence of this is that some of AMPS's conveniences that allow "save and reload" of your resources are
not going to work once you start compiling your front-end source.

To overcome this, you will need to change your plugin's AMPS configuration and tell it where it will find
your production-ready code. Add the following block to the `<configuration>` of the
`maven-amps-plugin`:

```xml
<systemPropertyVariables>
    <plugin.resource.directories>${project.build.outputDirectory}</plugin.resource.directories>
</systemPropertyVariables>
```

What's the `${project.build.outputDirectory}` bit? That's where Maven will output all of the project's compiled
assets -- by default, it is `target/classes`. In other words, that should be pointing to where our Gulp task is
compiling everything to ;)

Now, when you are running your plugin in a development environment, the source code for your
front-end resources won't be served via the product; your compiled code will.

You can test this out by running `atlas-debug` for your plugin, making a change to one of the front-end resources,
then running `atlas-mvn package` afterwards.

But wait: why do you need to run the `atlas-mvn package`? That seems a bit silly, right? Read on!

### Re-compiling your front-end code when the source changes

When an Atlassian product is in development mode, it will look for front-end resources in any of the configured
`plugin.resource.directories` locations on your filesystem, as opposed to looking inside a plugin's JAR.

By making the previous change, we have made a subtle yet important change to how our plugin is treated in development.
When our front-end source code *was* our production code, AMPS was automatically reloading it for us whenever we
refreshed the page. Now, it's looking at the compiled code, which isn't changing unless we compile the plugin!

So, if we want our front-end to refresh like it used to when we saved the source and reloaded the browser, it's now
up to us to automatically re-compile our code whenever the source changes. Once the code in the
`plugin.resource.directories` location gets updated, AMPS's magic will take care of loading it in the browser for us.

To that end, you will need a "watch" task for your front-end code.

#### Watching front-end code for changes

Gulp will let you create a watch task quite trivially:

```js
// gulpfile.js

/**
 * Kicks off the transpilation and minification of the plugin's javascript code.
 * The `jsFiles` variable and transpilation function were defined in the previous tutorial.
 */
function watchJs() {
    gulp.watch(jsFiles, transpileAndMinifyJs);
}

gulp.task("watch", watchJs);

```

Now, whenever you run `gulp watch` and modify a JS file, the front-end build process will automatically happen.

##### Preventing in-flight changes from killing the watch task

If a file is mid-way through development and your favourite code editor periodically saves your files automatically,
your source code will occasionally contain invalid syntax. That invalid syntax won't stop your watch task from
kicking off the build process for you; in such a case, the build might fail, which would also kill your watch task.

While it's undesirable for the watch task to fail during development, it's also undesirable for any invalid syntax
to get committed and "compiled" in to your final plugin! If invalid code were to be committed in your project, before
your final plugin could be built, you *would* want the build to fail.

So, you probably want to ignore compilation failures when your watch task is run, but only in that case.

To do this, you'll need to install some Gulp plugins for your project.
Run `yarn add --dev gulp-if gulp-plumber` to add them as a dependency to your project.

Next, you can update your Gulp tasks to handle errors appropriately:

```js
var gulpIf = require('gulp-if');
var gulpPlumber = require('gulp-plumber');
// all the other gulpfile's dependencies should stay around here

function errorHandler(ignoreErrors) {
    // we want to be lenient on syntax and compilation errors in development,
    // but harsh when building our plugin for distribution. Therefore,
    // we only activate plumber if we're running gulp via our watch task.
    return gulpIf(ignoreErrors, plumber());
}

function transpileAndMinifyJs(ignoreErrors) {
    return gulp.src(jsFiles)
        .pipe(errorHandler(ignoreErrors))
        .pipe(babel())
        .pipe(gulp.dest(MVN_OUTPUT_DIR))
        .pipe(uglify())
        .pipe(rename({ suffix: '-min' }))
        .pipe(gulp.dest(MVN_OUTPUT_DIR));
}

function watchJs() {
    gulp.watch(jsFiles, transpileAndMinifyJs.bind(true));
}

var processResources = gulp.parallel(transpileAndMinifyJs.bind(false));

gulp.task("build", processResources);
gulp.task("watch", watchJs);
```

Now if you have un-compilable JavaScript source code things will work as you want:

* when you run `gulp build` (or `atlas-mvn package`), the erroneous source will cause the build to fail.
* when you run `gulp watch`, any erroneous source will write a red log message, but the source will continue
  to be watched and built as it changes.

##### Automatically watching front-end code when developing

Though this watch task is useful, it isn't run automatically when `atlas-debug` is run. When developing your plugin,
this trade-off may be acceptable for you. If not, you can consider running the `maven-exec-plugin` in order to
asynchronously kick off the watch task.

Add the following Maven plugin code to the `<build>` section of your `pom.xml` file:

```xml
<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>exec-maven-plugin</artifactId>
    <version>1.5.0</version>
    <executions>
        <execution>
            <id>watch-frontend-files-when-running-the-app</id>
            <phase>package</phase>
            <goals>
                <goal>exec</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
        <async>true</async>
        <asyncDestroyOnShutdown>true</asyncDestroyOnShutdown>
        <executable>yarn</executable>
        <arguments>
            <argument>run</argument>
            <argument>watch</argument>
        </arguments>
        <workingDirectory>${project.basedir}</workingDirectory>
    </configuration>
</plugin>
```

You will also need to add a script to your `package.json` file called "watch" in order to guarantee that
the right version of Gulp is called, even in different development environments:

```json
{
  "scripts": {
    "watch": "gulp watch"
  }
}
```

There are a number of caveats with this approach to running the watch task:

* This is *not* the version of Yarn that was downloaded by the `frontend-maven-plugin`; this configuration assumes
  that `yarn` is an executable binary on a developer's machine.
* If `yarn` isn't there, this async task will do nothing, and it will fail *silently*!

Working around these shortcomings is beyond the scope of this tutorial, but is something you should be mindful of.

### Making plugin changes quickly at development time

AMPS ships with a plugin called [Quickreload][302], which offers a lot of development-time conveniences.
It makes re-loading a plugin in to a running Atlassian product as simple as running `atlas-mvn package`, and sometimes
even simpler!

To make use of Quickreload, you'll need to enable it in your plugin's AMPS configuration. Add the following lines
to the `<configuration>` of the `maven-amps-plugin`:

```xml
<enableFastdev>false</enableFastdev>
<enableQuickReload>true</enableQuickReload>
<quickReloadVersion>2.0.0</quickReloadVersion>
```

Quickreload is important for all the non-front-end code changes you might make.
For example, if you add new `<web-resource>` definitions or change existing ones in your `atlassian-plugin.xml` file,
they will only take effect when you rebuild your plugin and upload it to your running development instance.
Quickreload reduces this process to being as simple as (re-)running `atlas-mvn package` whenever you change your
non-front-end code.

### A final look at the configuration

Taking in to account the previous sections of this tutorial, your plugin's full AMPS configuration may
look similar to the one found in this tutorial repository:

```xml
    <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>maven-confluence-plugin</artifactId>
        <version>6.2.11</version>
        <extensions>true</extensions>
        <configuration>
            <compressResources>false</compressResources>
            <enableFastdev>false</enableFastdev>
            <enableQuickReload>true</enableQuickReload>
            <quickReloadVersion>2.0.0</quickReloadVersion>
            <productVersion>5.8.9</productVersion>
            <productDataVersion>5.8.9</productDataVersion>
            <systemPropertyVariables>
                <!--
                    We need to tell AMPS to stop looking at our source now, otherwise
                    if we run `atlas-run` or `atlas-debug` to check our plugin, it will
                    load our code out of src/main/resources, which would be very confusing!
                -->
                <plugin.resource.directories>${project.build.outputDirectory}</plugin.resource.directories>
            </systemPropertyVariables>
        </configuration>
    </plugin>
```

### Next steps

So now you can use a modern JavaScript language at build-time, you have a good save-and-reload development loop
to work within a running product, and most changes to your plugin are a breeze to make.

What about modern front-end code management? And what about building a more optimal and performant front-end?
You can learn more about this in the next tutorial, [Bundling server add-on UIs with Webpack][905].

[302]: https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload
[900]: 04-serve-compiled-ui-code.md
[905]: https://bitbucket.org/serverecosystem/sao4fed-bundle-the-ui
