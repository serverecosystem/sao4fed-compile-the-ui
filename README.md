# Server Add-Ons for Front-End Developers Tutorial
## Compiling the UI of a Server Add-On

This tutorial demonstrates how to compile the front-end code of an Atlassian Server add-on.

The codebase demonstrates compiling the [ES2015 version of JavaScript][101] down to [ES5][102] using [Babel.js][103]. The front-end compilation is controlled using [Gulp.js][104].

The codebase aims to demonstrate *how* you can compile your front-end code, *not the technologies to do so*. The front-end technology choices made here can be substituted for your own -- e.g., [TypeScript][105] instead of ES2015, [Grunt.js][106] instead of Gulp.js, [NPM][107] instead of [Yarn][108], and so on.


## Getting started 

To build or run this add-on, you'll need to install Java and the Atlassian SDK. You can learn how to [set up the Atlassian SDK and build a project][2] on [developer.atlassian.com][1].

### Building the add-on

Once you have the Atlassian SDK installed and working, the only things you should have to do are: 

1. Open a terminal window or command prompt
2. Navigate to the checkout directory for this repository
2. Call `atlas-mvn package`

### See it in action

Once the add-on is built -- or indeed, to build the add-on and then run it -- you can call `atlas-run`.


## Tutorial

By completing this tutorial, you will know how to:

* Hook in to the build process of your add-on to change how your front-end code is prepared for production.
* Compile your front-end code using a front-end build management tool, such as Gulp.js.
* Write your add-on code in a different version of JavaScript, such as ES2015.

You can read more on [the rationale for this tutorial][901] in the docs.

[Get started with the step-by-step tutorial][902]!



[1]: https://developer.atlassian.com
[2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project
[101]: https://github.com/lukehoban/es6features
[102]: https://en.wikipedia.org/wiki/ECMAScript#5th_Edition
[103]: https://babeljs.io/
[104]: http://gulpjs.com/
[105]: https://www.typescriptlang.org/
[106]: https://gruntjs.com/
[107]: https://docs.npmjs.com/getting-started/what-is-npm
[108]: https://yarnpkg.com/en/docs/getting-started
[901]: docs/01-why.md
[902]: docs/02-intro.md
