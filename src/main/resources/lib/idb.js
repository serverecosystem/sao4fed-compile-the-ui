(function() {
    'use strict';
    const idb = window.idb;
    delete window.idb;

    define('conf-glossary/lib/idb', () => idb);
})();
