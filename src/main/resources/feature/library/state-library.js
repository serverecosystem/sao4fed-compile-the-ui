define('conf-glossary/feature/library/state-library', [
    'conf-glossary/lib/backbone',
    'conf-glossary/lib/idb'
], function (Backbone, idb) {
    'use strict';

    /** The unique identifier of our IndexedDB database. */
    const STORE_KEY = 'conf-glossary';
    /** The current schema version of our IndexedDB database. */
    const DB_VERSION = 1;

    // Create a promise interface to IndexedDB for our library of terms.
    const dbPromise = idb.open('conf-glossary-store', DB_VERSION, upgradeDb => {
        upgradeDb.createObjectStore(STORE_KEY, {keyPath: 'id'});
    });

    // We only want to allow persistence of Backbone Models, so here I wrap the translation
    // between what Backbone knows about the term and what IndexedDB wants to know.
    function storeBackboneModelAttributes(dbTransaction, model) {
        if (model instanceof Backbone.Model === false) return;
        // Translate and persist the backbone model.
        dbTransaction.objectStore(STORE_KEY).put({
            id: model.id,
            data: model.toJSON()
        });
    }

    function loadBackboneModelAttributes(dbItem) {
        return dbItem.data;
    }

    return {
        load: function() {
            return dbPromise.then(db => {
                const loadResults = db.transaction(STORE_KEY).objectStore(STORE_KEY).getAll();
                return loadResults.then(items => items.map(loadBackboneModelAttributes));
            });
        },

        /**
         * Save a single item to the library.
         * @param {Backbone.Model} item - the item to save
         * @returns {Promise.<TResult>}
         */
        save: function(item) {
            return dbPromise.then(db => {
                const tx = db.transaction(STORE_KEY, 'readwrite');
                storeBackboneModelAttributes(tx, item);
                return tx.complete;
            });
        },

        /**
         * Save multiple items to the library at once.
         * @param {Array<Backbone.Model>} items - an array of items to save
         * @returns {Promise.<TResult>}
         */
        saveAll: function(items) {
            // Ensure we're working with an array of items.
            items = [].concat(items);

            // Persist each item to the library
            return dbPromise.then(db => {
                const tx = db.transaction(STORE_KEY, 'readwrite');
                let i, ii;
                for(i=0, ii=items.length; i<ii; i++) {
                    storeBackboneModelAttributes(tx, items[i]);
                }
                return tx.complete;
            });
        }
    };
});
