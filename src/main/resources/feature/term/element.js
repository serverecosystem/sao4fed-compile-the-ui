define('conf-glossary/feature/term/element', [
    'conf-glossary/feature/term-dialog/term-dialog',
    'conf-glossary/lib/skate'
], function(TermDialog, skate) {
    'use strict';

    return skate('conf-term', {
        'extends': 'dfn',
        'created': function(el) {
            console.log('term created', el);
        },
        'attached': function(el) {
            console.log('term attached to DOM', el);
        },
        'attributes': {
            'title': function(el, changes) {
                el.textContent = changes.newValue;
            }
        },
        'events': {
            'click': function(el, e) {
                e.preventDefault();
            },
            'mouseenter': function(el, e) {
                e.preventDefault();
                TermDialog.showTermPopup(el, e);
            },
            'mouseleave': function(el, e) {
                e.preventDefault();
                TermDialog.closeTermPopup(el);
            }
        }
    })
});
