define('conf-glossary/feature/glossary-notifications/glossary-notifications', [
    'conf-glossary/feature/glossary/glossary',
    'aui/flag',
    'jquery'
], function(glossary, flag, $) {

    glossary.on('sync', (...args) => {
        console.log('glossary was synced', args);
    });

    glossary.on('add', model => {
        console.log('term added', model);
    });

    glossary.on('persisted', response => {
        console.log('persisted', response);
        const f = flag({
            type: 'success',
            title: 'Term definition saved!',
            persistent: false,
            body: 'Thanks for making <conf-term>Confluence</conf-term> a more legible place! &#x1f64c;'
        });
        setTimeout(f.close, 5000);
    });
});
