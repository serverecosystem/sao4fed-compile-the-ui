/**
 * TODO: Should be custom element, or inline dialog.
 * TODO: Should get created by (and instance owned by) the term element itself.
 */
define('conf-glossary/feature/term-dialog/term-dialog', ['require'], function(require) {
    const Meta = AJS.Meta;
    const glossary = require('conf-glossary/feature/glossary/glossary');
    const _ = require('conf-glossary/lib/underscore');
    const $ = require('jquery');

    const $noTerm = $("<div></div>");
    const space = Meta.get('space-key');

    function getTermByName(name) {
        return glossary.findByTerm(name);
    }

    const dfnTemplate = _.template('<li <% if (data.scope) { %>data-scope="<%= data.scope %>"<% } %>><p><%= data.content %><% if (data.url && data.url.length) { %> &mdash; <a href="<%= data.url %>">more...</a><% } %></p></li>', undefined, {variable:"data"});
    const popupElement = _.template('<div class="glossary-term-hover" id="<%= term.id %>"></div>');
    const popupContents = _.template(`
        <h3><%= term.name %></h3>
        <% if (term.grouped[space] && term.grouped[space].length) { %>
        <ul class="definitions this-space"><% _.each(term.grouped[space], function(dfn) { %><%= dfnTemplate(dfn) %><% }); %></ul>
        <% } %>
        <ul class="definitions everywhere"><% _.each(term.grouped["_"], function(dfn) { %><%= dfnTemplate(dfn) %><% }); %></ul>
        `);

    function createTermPopup(term) {
        const id = term.domId();
        let $el = $(document.getElementById(id));
        const data = _.clone(term.attributes);
        data.id = id;
        data.grouped = _.groupBy(term.definitions(), dfn => (dfn.scope ? dfn.scope : "_"));
        const html = popupContents({ term: data, space: space, dfnTemplate: dfnTemplate });
        if (!$el.length) {
            $el = $(popupElement({term: data})).html(html);
            $el.bind("mouseenter", e => { onEnterPopup($el); });
            $el.bind("mouseleave", e => { onLeavePopup($el); });
        } else {
            $el.html(html);
        }
        $el.appendTo(document.body);
        return $el;
    }

    function getTermPopup(termName) {
        const term = getTermByName(termName);
        if (!term) return $noTerm;
        term.popup = createTermPopup(term);
        return term.popup;
    }

    function onEnterPopup(popup) {
        const $popup = (popup.jquery) ? popup : $(popup);
        clearTimeout($popup.data("closing"));
        $popup.data("closing", null);
    }

    function onLeavePopup(popup) {
        const $popup = (popup.jquery) ? popup : $(popup);
        clearTimeout($popup.data("closing"));
        $popup.data("closing", setTimeout(() => { hidePopup($popup) }, 1000));
    }

    function hidePopup($popup) {
        if ($popup && $popup.data("closing")) {
            $popup.hide("slow");
        }
    }

    function showTermPopup(element, e) {
        const $el = $(element);
        const termName = $el.attr("title");
        const $popup = getTermPopup(termName);
        $popup.hide();
        $popup.css({
            "position": "fixed",
            "top": e.clientY,
            "left": e.clientX
        });
        $popup.show();
        onEnterPopup($popup);
    }

    function closeTermPopup(element) {
        const $el = $(element);
        const termName = $el.attr("title");
        const $popup = getTermPopup(termName);
        onLeavePopup($popup);
    }

    function hideAllPopupsRightNow() {
        $(".glossary-term-hover").each(function() {
            const $popup = $(this);
            clearTimeout($popup.data("closing"));
            $popup.hide();
        });
    }


    return {
        showTermPopup,
        closeTermPopup,
        hideAll: hideAllPopupsRightNow
    }
});
