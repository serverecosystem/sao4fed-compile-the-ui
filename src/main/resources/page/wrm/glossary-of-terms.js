/* global WRM */
WRM.require(['wrc!glossary-of-terms'], function() {
    // Invoke our module now that it's been loaded by the WRM.
    window.require(['conf-glossary/glossary-of-terms']);
});
