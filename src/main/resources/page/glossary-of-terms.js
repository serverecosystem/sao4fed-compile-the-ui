define('conf-glossary/glossary-of-terms', [
    'conf-glossary/widget/highlight-dialog',
    'conf-glossary/widget/highlight-action',
    'conf-glossary/feature/term-linker/term-linker',
    'conf-glossary/feature/term-dialog/term-dialog',
    'conf-glossary/feature/glossary/glossary',
    'jquery',
    'conf-glossary/feature/glossary-notifications/glossary-notifications'
], function (HighlightDialog, HighlightAction, TermLinker, TermDialog, glossary, $) {
    const PLUGIN_KEY = 'com.atlassian.confluence.plugins.glossary-of-terms:create-definition';

    //
    // Make ourselves a highlight dialog.
    // Ensure that we save all terms that are submitted through it.
    //

    const dialog = new HighlightDialog();
    dialog.onSubmit(data => {
        glossary.add({
            name: data['term-name'],
            definition: data['term-definition'],
            scope: data['term-scope']
        }, { parse: true, merge: true }); // TODO 9: really need to make this data a proper collection
        glossary.persist();
    });

    //
    // Hook up an action on the little popup that appears when users
    // highlight words in the document.
    //

    HighlightAction.registerButtonHandler(PLUGIN_KEY, {
        onClick: function(selectionObject) {
            dialog.showHighlightDialog(selectionObject);
            TermDialog.hideAll();
        },
        shouldDisplay: HighlightAction.WORKING_AREA.MAINCONTENT_ONLY
    });


    // Hook the term-linker up to our Glossary
    // so it'll define words as we learn of them.

    glossary.on('add', (term, ...args) => {
        console.log('glossary was changed', [term, ...args]);
        TermLinker.addTermsToPage([term]);
    });

    //
    // Why not just start it now, too? :D
    //
    try {
        glossary.fetch();
    } finally {
        // We weep softly,
        // for the meaning of our terms
        // is lost to us now.
    }

    // Initialise when DOM Content Loaded has happened.
    // This could also be fired on other events, like when
    // comments are added or edited.
    $(() => {
        TermLinker.addTermsToPage(glossary.models);
    });
});
